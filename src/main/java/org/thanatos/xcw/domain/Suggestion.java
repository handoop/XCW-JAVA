package org.thanatos.xcw.domain;

import org.apache.solr.client.solrj.beans.Field;

/**
 * Created by thanatos on 15-10-14.
 */
public class Suggestion {

    //field
    private String suggest;
    private String pinwords;
    private String abbrwords;

    //hashcode and equals


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Suggestion that = (Suggestion) o;

        return suggest != null ? suggest.equals(that.suggest) : that.suggest == null;
    }

    @Override
    public int hashCode() {
        int result = suggest != null ? suggest.hashCode() : 0;
        result = 31 * result + (pinwords != null ? pinwords.hashCode() : 0);
        result = 31 * result + (abbrwords != null ? abbrwords.hashCode() : 0);
        return result;
    }

    //get and set
    public String getSuggest() {
        return suggest;
    }

    @Field("suggest")
    public void setSuggest(String suggest) {
        this.suggest = suggest;
    }

    public String getPinwords() {
        return pinwords;
    }

    @Field("pinwords")
    public void setPinwords(String pinwords) {
        this.pinwords = pinwords;
    }

    public String getAbbrwords() {
        return abbrwords;
    }

    @Field("abbrwords")
    public void setAbbrwords(String abbrwords) {
        this.abbrwords = abbrwords;
    }
}
