package org.thanatos.xcw.domain;

import org.apache.ibatis.type.Alias;
import org.apache.solr.client.solrj.beans.Field;

/**
 * Created by Administrator on 2015/12/9.
 */
@Alias("Goods")
public class Goods {

    // ----fields----
    private Long id;
    private String title;
    private Long type;

    // ---- set and get ----
    public Long getId() {
        return id;
    }

    @Field("goods_id")
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    @Field("goods_title")
    public void setTitle(String title) {
        this.title = title;
    }

    public Long getType() {
        return type;
    }

    @Field("goods_type")
    public void setType(Long type) {
        this.type = type;
    }
}
