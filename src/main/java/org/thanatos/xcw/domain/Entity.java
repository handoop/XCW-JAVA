package org.thanatos.xcw.domain;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/11/23.
 */
public class Entity implements Serializable{

    // fields
    private Long id;

    // get and set
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
