
namespace java org.thanatos.xcw.rpc
/**
  * @author Administrator
  * @date 2015-11-23
  **/
service IUserService{

    /**
    * 用户注册
    **/
    bool signin(1:string username, 2:string nick, 3:string password)

    /**
    * 用户登录
    **/
    string login(1:string username, 2:string password)

    /**
    * 通过Nick得到用户的信息
    **/
    string getUserByNick(1:string username)

    /**
    * 更新用户信息
    **/
    bool updateInfo(1:string userstr)

    /**
    * 更新用户头像
    **/
    bool update4portrait(1:string portrait)

    /**
    * 好友列表
    **/
    list<string> friends(1:i64 uid, 2:i32 pageNum)



}