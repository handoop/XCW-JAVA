
/**
  * @author Administrator
  * @date 2015-11-25
  **/   
service ILetterService{

    /**
    * 得到用户私信列表
    **/
    string getUserLetters(1:i64 uid, 2:i32 pageNum)

    /**
    * 得到未读私信的数量
    **/
    i32 getNotRead(1:i64 uid)

    /**
    * 清空未读私信
    **/
    bool clearNotRead(1:i64 uid)

    /**
    * 保存私信
    **/
    i64 save(1:string letterstr)

}