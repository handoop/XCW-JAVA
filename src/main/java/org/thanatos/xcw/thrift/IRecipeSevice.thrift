namespace java org.thanatos.xcw.api
/**
  * @author Administrator
  * @date 2015-11-23
  **/
service IRecipeService{

 /**
 *   1、发布菜谱
 */
    string addRecipe(1:string recipeJSON)

 /**
 *   2、删除菜谱
 */
    string deleteRecipe(1:string id)

 /**
 *   3、修改菜谱
 */
    string updateRecipe(1:string recipeJSON)

/**
 *   4、查询菜谱
 */
    string checkRecipe(1:string id)

/**
 *   5、评论菜谱
 */
    string doComment(1:string comment)

/**
 *   6、点赞菜谱
 */
    string doVote(1:string id)

/**
 *   7、收藏菜谱
 */
    string collectRecipe(1:string id)

}