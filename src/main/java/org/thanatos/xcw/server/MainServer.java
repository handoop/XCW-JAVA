package org.thanatos.xcw.server;


import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportFactory;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.thanatos.xcw.rpc.IGoods;
import org.thanatos.xcw.rpc.ISearcher;
import org.thanatos.xcw.rpc.IUser;

/**
 * Created by handoop on 2015/5/13.
 */
@SuppressWarnings("all")
public class MainServer {

//    public static IUser.Iface iUser;
    public static ISearcher.Iface iSearcher;
    public static IGoods.Iface iGoods;

    public static void main(String[] _args) throws Exception {
        //init spring
        GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.setValidating(false);
        context.load("classpath:spring-context.xml");
        context.refresh();

        //ioc
//        iUser = context.getBean(IUser.Iface.class);
        iSearcher = context.getBean(ISearcher.Iface.class);
        iGoods = context.getBean(IGoods.Iface.class);

        //thrift api
        TMultiplexedProcessor processor = new TMultiplexedProcessor();
        TServerTransport t = new TServerSocket(9090);
        TThreadPoolServer.Args args = new TThreadPoolServer.Args(t);
        args.processor(processor);
        args.protocolFactory(new TBinaryProtocol.Factory());
        args.transportFactory(new TTransportFactory());
        TServer server = new TThreadPoolServer(args);

        //register service
//        processor.registerProcessor("IUser", new IUser.Processor<IUser.Iface>(iUser));
        processor.registerProcessor("ISearcher", new ISearcher.Processor<ISearcher.Iface>(iSearcher));
        processor.registerProcessor("IGoods", new IGoods.Processor<IGoods.Iface>(iGoods));

        //run
        System.out.println("MainServer app is listening port 9090......");
        server.serve();
    }

}
