package org.thanatos.xcw.service.imp;

import com.alibaba.fastjson.JSON;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thanatos.xcw.domain.Goods;
import org.thanatos.xcw.rpc.IGoods;

/**
 * Created by thanatos on 15-9-13.
 */
@Service
@Transactional(rollbackFor = TException.class)
public class IGoodsImp implements IGoods.Iface{

    private static final Logger logger = LoggerFactory.getLogger("IGoodsImp");
    @Autowired SolrServer mSolrServer;

    @Override
    public String saveToSolr(String obj) throws TException {
        try {
            Goods goods = JSON.toJavaObject(JSON.parseObject(obj), Goods.class);
            mSolrServer.addBean(goods, -1);
            mSolrServer.commit();
            return "";
        }catch (Exception e){
            e.printStackTrace();
            try {
                mSolrServer.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            throw new TException(e);
        }
    }

    @Override
    public void delete4solr(long id, int type) throws TException{
        try {
            mSolrServer.deleteByQuery(String.format("goods_id:%d AND goods_type:%d", id, type));
        } catch (Exception e) {
            e.printStackTrace();
            try {
                mSolrServer.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

}
